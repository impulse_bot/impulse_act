package com.resatapi.impulseact.controller;

import com.resatapi.impulseact.model.Todo;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertEquals;

public class RestAPIControllerTest {

    @Autowired
    private MockMvc mockMvc;

//    // Create a new Todo
//    @PostMapping("/todos/")
//    public HashMap<String, Object> createTodo(@Valid @RequestBody Todo newTodo) {
//        try {
//            return RequestUtil.sendRequest(true, todoRepo.save(newTodo));
//        } catch (Exception e) {
//            return RequestUtil.sendRequest(false, e.toString());
//        }
//    }

//    this.id = id;
//        this.userId = userId;
//        this.todoName = todoName;
//        this.date = date;
//        this.timeStart = timeStart;
//        this.timeEnd = timeEnd;
//        this.completion = completion;
//        this.importance = importance;

    @Before
    public void setUp() throws Exception{
        Todo todo = new Todo();
        todo.setId("1234567dfghjmk");
        todo.setUserId("line123ert");
        todo.setTodoName("Makan cereal");
        todo.setDate("2019-05-22");
        todo.setTimeStart("08:00");
        todo.setTimeEnd("08:30");
        todo.setCompletion(false);
        todo.setImportance(true);

    }
    @Test
    public void createProduct() throws Exception {
        String uri = "/todos/";

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        assertEquals(content, "Product is created successfully");
    }
}

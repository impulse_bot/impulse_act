package com.resatapi.impulseact.model;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest()
public class TodoTest {

    Todo todo;
    Todo todo1;

    @Before
    public void setUp() throws Exception {
        todo = new Todo();
        todo.setId("12345678908752");
        todo.setUserId("A1234B");
        todo.setTodoName("Belajar Adpro");
        todo.setDate("2019-04-12");
        todo.setTimeStart("20:00");
        todo.setTimeEnd("22:00");
        todo.setCompletion(false);
        todo.setImportance(true);

        todo1 = new Todo("0987654321234", "B12345A", "Belajar Masak", LocalDate.of(2019,04,12),  LocalTime.of(10,00), LocalTime.of(12,00) , false, true);
    }

    @Test
    public void testGetTodoId() {
        assertEquals(todo.getId(), "12345678908752");
        assertEquals(todo1.getId(), "0987654321234");
    }

    @Test
    public void testGetUserId() {
        assertEquals(todo.getUserId(), "A1234B");
        assertEquals(todo1.getUserId(), "B12345A");
    }

    @Test
    public void testGetTodoName() {
        assertEquals(todo.getTodoName(), "Belajar Adpro");
        assertEquals(todo1.getTodoName(), "Belajar Masak");
    }

    @Test
    public void testGetDate() {
        assertEquals(todo.getDate(), "2019-04-12");
        assertEquals(todo1.getDate(), "2019-04-12");
    }

    @Test
    public void testGetTimeStart() {
        assertEquals(todo.getTimeStart(),"20:00" );
        assertEquals(todo1.getTimeStart(),"10:00" );
    }

    @Test
    public void testGetTimeEnd() {
        assertEquals(todo.getTimeEnd(), "22:00");
        assertEquals(todo1.getTimeEnd(), "12:00");
    }

    @Test
    public void testGetCompletion() {
        assertEquals(todo.getCompletion(), false);
        assertEquals(todo1.getCompletion(), false);
    }

    @Test
    public void testGetImportance() {
        assertEquals(todo.getImportance(), true);
        assertEquals(todo1.getImportance(), true);
    }
}


package com.resatapi.impulseact.model;


import org.hibernate.annotations.GenericGenerator;
import java.util.Locale;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import java.time.format.DateTimeFormatter;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name = "todo")
public class Todo {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id")
    private String id;

    @NotBlank
    @Column(name="user_id")
    private String userId;

    @NotBlank
    @Column(name = "todo_name")
    private String todoName;

    @NotNull
    @Column(name = "date")
    private LocalDate date;

    @NotNull
    @Column(name = "time_start")
    private LocalTime timeStart;

    @NotNull
    @Column(name = "time_end")
    private LocalTime timeEnd;

    @NotNull
    @Column(name = "completion")
    private boolean completion;

    @NotNull
    @Column(name = "importance")
    private boolean importance;

    public Todo() {
        super();
    }

    public Todo(String id, String userId, String todoName, LocalDate date, LocalTime timeStart, LocalTime timeEnd, boolean completion, boolean importance) {
        super();
        this.id = id;
        this.userId = userId;
        this.todoName = todoName;
        this.date = date;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.completion = completion;
        this.importance = importance;

    }

    public String getTodoName() {
        return this.todoName;
    }

    public void setTodoName(String todoName) {
        this.todoName = todoName;
    }

    public String getTimeStart() {
        return this.timeStart.toString();
    }

    public void setTimeStart(String timeStartParam) throws Exception {
        LocalTime timeStart =  LocalTime.parse(timeStartParam);
        this.timeStart = timeStart;
    }

    public String getTimeEnd() {
        return this.timeEnd.toString();
    }

    public void setTimeEnd(String timeEndParam) throws Exception {
        LocalTime timeEnd =  LocalTime.parse(timeEndParam);
        this.timeEnd = timeEnd;
    }

    public String getDate() {
        return this.date.toString();
    }

    public void setDate(String dateParam) throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
        LocalDate date = LocalDate.parse(dateParam, formatter);
        this.date = date;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean getCompletion() {
        return this.completion;
    }

    public void setCompletion(Boolean completion) {
        this.completion = completion;
    }

    public boolean getImportance() {
        return this.importance;
    }

    public void setImportance(Boolean importance) {
        this.importance = importance;
    }

}
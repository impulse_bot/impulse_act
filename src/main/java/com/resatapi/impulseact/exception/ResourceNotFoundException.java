package com.resatapi.impulseact.exception;

public class ResourceNotFoundException extends Exception {
    private long todo_id;
    public ResourceNotFoundException(String todo_id) {
        super(String.format("Todo is not found with id : '%s'", todo_id));
    }
}

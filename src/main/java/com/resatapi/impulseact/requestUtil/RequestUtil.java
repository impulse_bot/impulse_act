package com.resatapi.impulseact.requestUtil;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class RequestUtil {

    public static HashMap<String, Object> sendRequest(boolean success, Object content) {
        HashMap<String, Object> response = new LinkedHashMap<String, Object>();
        response.put("success", success);
        if (success) {
            response.put("data", content);
        } else {
            response.put("message", content);
        }
        return response;
    }
}

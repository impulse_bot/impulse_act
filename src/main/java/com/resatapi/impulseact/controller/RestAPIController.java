package com.resatapi.impulseact.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.resatapi.impulseact.exception.ResourceNotFoundException;
import com.resatapi.impulseact.model.Todo;
import com.resatapi.impulseact.repository.TodoRepository;
import com.resatapi.impulseact.requestUtil.RequestUtil;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

import java.util.HashMap;

@RestController
public class RestAPIController {


    @Autowired
    TodoRepository todoRepo;

    // show all userId
    @PostMapping("/all_userid/")
    public HashMap<String, Object> showAllUserID() {
        try {
            return RequestUtil.sendRequest(true, todoRepo.showAllUserID());
        } catch (Exception e) {
            return RequestUtil.sendRequest(false, e.toString());
        }
    }

    // Create a new Todo
    @PostMapping("/todos/")
    public HashMap<String, Object> createTodo(@Valid @RequestBody Todo newTodo) {
        try {
            return RequestUtil.sendRequest(true, todoRepo.save(newTodo));
        } catch (Exception e) {
            return RequestUtil.sendRequest(false, e.toString());
        }
    }

    // Get a Single Todo
    @GetMapping("/todos/{id}")
    public HashMap<String, Object> getTodoById(@PathVariable(value = "id") String todoId) {
        try {
            return RequestUtil.sendRequest(true, todoRepo.findById(todoId));
        } catch (Exception e) {
            return RequestUtil.sendRequest(false, e.toString());
        }
    }

    //Get all Todo from a single user
    @GetMapping("/user_todos/{id}")
    public HashMap<String, Object> getTodoByUserId(@PathVariable(value = "id") String userId) {
        try {
            return RequestUtil.sendRequest(true, todoRepo.findAllTodoOfUser(userId));
        } catch (Exception e) {
            return RequestUtil.sendRequest(false, e.toString());
        }
    }

    // Update a Todo
    @PutMapping("/todos/{id}")
    public HashMap<String, Object> updateTodo(@PathVariable(value = "id") String todoId, @Valid @RequestBody Todo todoDetails) {
        try {
            Todo todo = todoRepo.findById(todoId).orElseThrow(() -> new ResourceNotFoundException(todoId));
            todo.setUserId(todoDetails.getUserId());
            todo.setTodoName(todoDetails.getTodoName());
            todo.setDate(todoDetails.getDate());
            todo.setTimeStart(todoDetails.getTimeStart());
            todo.setTimeEnd(todoDetails.getTimeEnd());
            todo.setCompletion(todoDetails.getCompletion());
            todo.setImportance(todoDetails.getImportance());
            return RequestUtil.sendRequest(true, todoRepo.save(todo));
        } catch (Exception e) {
            return RequestUtil.sendRequest(false, e.toString());
        }
    }

    // Delete a Todo
    @DeleteMapping("/todos/{id}")
    public HashMap<String, Object> deleteTodo(@PathVariable(value = "id") String todoId) {
        try {
            Todo todo = todoRepo.findById(todoId).orElseThrow(() -> new ResourceNotFoundException(todoId));
            todoRepo.delete(todo);
            return RequestUtil.sendRequest(true, ResponseEntity.ok().build());
        }  catch (Exception e) {
            return RequestUtil.sendRequest(false, e.toString());
        }
    }
}




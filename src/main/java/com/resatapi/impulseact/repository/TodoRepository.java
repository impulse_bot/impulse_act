package com.resatapi.impulseact.repository;

import com.resatapi.impulseact.model.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.query.Param;

import java.util.List;

@Repository
public interface TodoRepository extends JpaRepository<Todo, String> {

    @Query("select t from todo t where t.user_id = :userId order by t.date, t.timeStart")
    List<Todo> findAllTodoOfUser(@Param("user_id") String userId);

    @Query("select distinct user_id from todo")
    List<String> showAllUserID();

}
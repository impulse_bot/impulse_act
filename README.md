# Impulse Act

[![pipeline status](https://gitlab.com/impulse_bot/impulse_act/badges/master/pipeline.svg)](https://gitlab.com/impulse_bot/impulse_act/commits/master) [![coverage report](https://gitlab.com/impulse_bot/impulse_act/badges/master/coverage.svg)](https://gitlab.com/impulse_bot/impulse_act/commits/master)

## Overview

Impulse is Todo Assistant LINE Bot made to help our users to control and manage their schedule.

Impulse Act is one out of two part of our Todo Assistant LINE Bot. The Act acts as the API and the database service for storing our todos. This service will act as the endpoint for our Todo�s data storage. It will return an API for other services to call to get requested data or to store a data given a certain request.

## Application Access

The Impulse LINE bot can be added through this link:  

<a href="https://line.me/R/ti/p/%40nit4592h"><img height="36" border="0" alt="Tambah Teman" src="https://scdn.line-apps.com/n/line_add_friends/btn/en.png"></a>

## Authors (A8)

* **Jonathan Christopher Jakub** - [Jonathanjojo](https://gitlab.com/Jonathanjojo)
* **Aryo Tinulardhi** - [Aryodh](https://gitlab.com/aryodh)
* **Nabila Febri Viola** - [nabilafv](https://gitlab.com/nabilafv)
* **Aziz FIkri Hudaya** - [Azizhudaya](https://gitlab.com/Azizhudaya)
* **Nadhifah Hanan** - [nadhifahhanan](https://gitlab.com/nadhifahhanan)

## Acknowledgements

* Advance Programming CS UI 2019

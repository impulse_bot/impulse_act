
CREATE TABLE todo
(
    id                VARCHAR NOT NULL,
    user_id           VARCHAR NOT NULL,
    todo_name         VARCHAR(500) NOT NULL,
    date              VARCHAR(254) NOT NULL,
    time_start        VARCHAR(254),
    time_end          VARCHAR(254),
    completion        VARCHAR(254),
    importance        VARCHAR(254)
);